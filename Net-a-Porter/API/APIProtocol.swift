//
//  APIProtocol.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import SwiftyJSON

/**
	A protocol to handle API objects as part of the Networking Library.

	Defines base properties needed to make the API call.
*/
protocol APIProtocol : Decodable {
	
	/// The name of this API call, for logging
	static var title: String { get }
	
	/// The URL for this API call
	static var urlString: String { get }
}

/**
	API Protocol Parameters
*/
protocol APIProtocolParameters {
	
	///    Returns parameters.
	///    You can scale this by adding type/ method to APIProtocol. For now I'll stick with URL encoded
	func parameters() -> String?
	
}
