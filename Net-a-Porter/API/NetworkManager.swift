//
//  NetworkManager.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import SDWebImage
import UIKit

/**
	Protocol for networking
*/
protocol Networking {
	func fetch<E: APIProtocol, P: APIProtocolParameters>(with type: E.Type, and parameters: P, completion: @escaping ((APIProtocolCompletion) -> Void))
}

/**
	The network manager handles all API calls to the Last.fm API
*/
class NetworkManager: Networking {
	
	/// A network sessions
	let networkSession: URLSession
	
	///    Configuration
	private var configuration: URLSessionConfiguration = {
		let sessionConfiguration = URLSessionConfiguration.default
		sessionConfiguration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
		
		return sessionConfiguration
	}()
	
	/**
		Initalise with a configuration
	*/
	init() {
		self.networkSession = URLSession(configuration: self.configuration, delegate: nil, delegateQueue: nil)
	}
	
	// MARK: Public Functions
	
	/**
		A generic API call to fetch from the API where objects should conform to decodable.
	*/
	func fetch<E: APIProtocol, P: APIProtocolParameters>(with type: E.Type, and parameters: P, completion: @escaping ((APIProtocolCompletion) -> Void)) {
		print("[NetworkManager] Fetching Data for Object \(String(describing: type))")
		
		var urlString = type.urlString
		if let params = parameters.parameters() {
			urlString += "\(params)"
		}
		guard let url = URL(string:urlString) else {
			print("Needed a valid URL")
			completion(APIProtocolCompletion(isSuccess: false))
			return
		}
		
		let dataTask = networkSession.dataTask(with: url) { (data, _, error) in
			
			guard let data = data, error == nil else {
				DispatchQueue.main.async {
					completion(APIProtocolCompletion(isSuccess: false))
				}
				return
			}
			
			/// Convert to object
			do {
				
				print("[NetworkManager] Successfully fetched data for \(type)")
				let object = try JSONDecoder().decode(type, from: data)
				DispatchQueue.main.async {
					let completionResponse = APIProtocolCompletion(isSuccess: true,
																   service: object)
					completion(completionResponse)
				}
				
			} catch {
				print("[NetworkManager] Error in catch \(String(describing: error))")
				DispatchQueue.main.async {
					let errorResponse = APIProtocolCompletion(isSuccess: false)
					completion(errorResponse)
				}
				return
			}
			
		}
		
		dataTask.resume()
	}

}
