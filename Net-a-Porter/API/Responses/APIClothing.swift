//
//  GenericResponse.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	APIClothing Type
*/
struct APIClothing: Decodable {
	
	///	An Id for the article of clothing
	let id: Int
	
	///	The name
	let name: String
	
	///	Price for this item of clothing
	let price: APIPrice
	
	///	If this item of clothing is visible
	public let isVisible: Bool
	
	///	Images for this item of clothing
	public let images: APIImages
	
	/**
		CodingKeys
	*/
	enum CodingKeys: String, CodingKey {
		case id
		case name
		case price
		case isVisible = "visible"
		case images
	}
}
