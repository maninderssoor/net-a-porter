//
//  APIImages.swift
//  Net-a-Porter
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	APIImages type
*/
struct APIImages: Decodable {
	
	///	An array of shots
	let shots: [String]
	
	///	An array of sizes
	let sizes: [String]
	
	///	URL Template
	let urlTemplate: String
}

