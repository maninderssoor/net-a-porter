//
//  APIPrice.swift
//  Net-a-Porter
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation


/**
	APIPrice Type
*/
struct APIPrice: Decodable {
	
	///	The Currency
	let currency: String
	
	///	The divisior
	let divisor: Int
	
	///	The amount
	let amount: Int
}
