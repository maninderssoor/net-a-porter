//
//  GenericService.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Create a Clothing API Service
*/
struct APIClothingService: APIProtocol {
	
	/// The name of this API call, for logging
	static var title: String = "Clothing"
	
	/// The URL for this call
	static var urlString: String = "\(Constants.hostURL.rawValue)60/0/summaries"

	///	An array of clothing object
	let clothing: [APIClothing]
	
	/**
		CodingKeys for the APISession
	*/
	enum CodingKeys: String, CodingKey {
		case clothing = "summaries"
	}
}

/**
	Clothing parameter
*/
struct APIClothingParameteres: APIProtocolParameters {
	
	/// The category ID
	let categoryID: Int
	
	func parameters() -> String? {
		return "?categoryIds=\(categoryID)"
	}
	
}
