//
//  AppDelegate.swift
//  Net-a-Porter
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import UIKit
import SDWebImage

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	/**
		Initialise any items
	*/
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		let _ = SDWebImageManager.shared
		
		return true
	}

}

