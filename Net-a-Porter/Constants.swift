//
//  Constants.swift
//  LastFM
//
//  Created by Maninder Soor on 19/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

enum Constants: String {
	
	/// Host name
	case hostURL = "https://api.net-a-porter.com/NAP/GB/en/"
	
	///	Lottie activity file
	case activity = "activity"
}


enum UITests: String {
	
	///	Collection view
	case collection
	
	///	The activity loader
	case activity
	
	///	Navigation header label
	case navigationHeaderLabel
	
	///	The image view
	case imageView
	
}
