//
//  Movie.swift
//  MovieDB
//
//  Created by Maninder Soor on 06/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

/**
	A Clothihng Object
*/
@objcMembers class Clothing: Object {
	
	///	An ID for the clothing item
	dynamic var id: Int = -1
	
	///	The name of the clothing item
	dynamic var name: String = ""
	
	///	The currency of the item
	dynamic var currency: String = ""
	
	///	The currency symbol
	dynamic var currencySymbol: String = ""
	
	///	The Price of the 
	dynamic var price: Float = 0.0
	
	///	The main image
	dynamic public var image: Image?
	
	/**
		Convenience Initialiser
	*/
	convenience init(id: Int, name: String, currency: String, currencySymbol: String, price: Float, image: Image?) {
		self.init()
		
		self.id = id
		self.name = name
		self.currency = currency
		self.currencySymbol = currencySymbol
		self.price = price
		self.image = image
	}
	
	
	/**
		Primary Key
	*/
	override class func primaryKey() -> String {
		return ClothingKey.id.rawValue
	}
	
}

/**
	Keys for the Clothing Object
*/
enum ClothingKey: String {
	case id
	case name
	case currency
	case price
	case image
}
