//
//  DataSources.swift
//  MovieDB
//
//  Created by Maninder Soor on 06/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

enum DataSource {
	
	///	The data retrieved comes from the API
	case api
	
	///	The data retrieved comes from the Database
	case database
}
