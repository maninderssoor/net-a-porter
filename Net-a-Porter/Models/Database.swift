//
//  Database.swift
//  Recipes
//
//  Created by Maninder Soor on 01/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

/**
	The database manager that offers convenience for fetching from the DB
*/
class Database {
	
	/**
		Fetch objects
	
	- parameter for: The type of Object being looked up
	- parameter filter: An NSPredicate String
	
	- returns: An array of objects, if they're found
	*/
	static func fetch(for type: Object.Type, filter predicate: NSPredicate? = nil) -> [Object]? {
		
		var objects = try? Realm().objects(type)
		
		if let predicate = predicate {
			objects = objects?.filter(predicate)
		}
		
		print("Found first object with predicate \(String(describing: predicate))")
		
		return objects?.toArray()
	}
	
	/**
	Adds to the default Realm
	
	- parameter with: The object to write to the default Realm
	
	- returns: A Realm Object, if one was inserted.
	*/
	static func addObjects(with objects: [Object]) {
		
		try? Realm().write {
			try? Realm().add(objects, update: true)
		}
	}
	
	/**
	Removes all from the default Realm
	
	- parameter with: The object to write to the default Realm
	
	- returns: A Realm Object, if one was inserted.
	*/
	@discardableResult
	static func delete(forType type: Object.Type) -> Bool {
		
		guard let results = Database.fetch(for: type), results.count > 0 else {
			print("Couldn't get any results")
			return false
		}
		
		try? Realm().write {
			try? Realm().delete(results)
		}
		return true
	}
}


/**
	Convert a Realm Results to an array
*/
extension Results {
	
	func toArray() -> [Results.Iterator.Element] {
		var array = [Element]()
		for item in self { array.append(item as Results.Iterator.Element) }
		
		return array
	}
}


extension List where Element: RealmSwift.Object {
	
	/**
	Appends the given object to the end of the list in a Realm write transaction.
	
	If the object is managed by a different Realm than the receiver, a copy is made and added to the Realm managing
	the receiver.
	
	- warning: This method may only be called during a write transaction.
	
	- parameter object: An object.
	*/
	public func appendAndWrite(_ object: Element) {
		
		///	Ignore if this object is already in the list
		let filtered = filter({ $0 == object })
		if filtered.count > 0 {
			return
		}
		
		try? Realm().write {
			self.append(object)
		}
	}
	
	/**
	Removes the given object to the end of the list in a Realm write transaction.
	
	- parameter object: An object.
	*/
	public func removeAndWrite(_ object: Element) {
		
		guard let firstIndex = self.firstIndex(where: { object == $0 }) else {
			return
		}
		
		try? Realm().write {
			self.remove(at: firstIndex)
		}
	}
}
