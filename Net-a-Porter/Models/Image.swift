//
//  Price.swift
//  Net-a-Porter
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

/**
	An Image Object
*/
@objcMembers class Image: Object {
	
	///	The urlString
	dynamic var urlString: String = ""
	
	/**
		Initialiser
	*/
	convenience init(urlString: String) {
		self.init()
		
		self.urlString = urlString
	}
	
	
	/**
		Primary Key
	*/
	public override class func primaryKey() -> String {
		return ImageKey.urlString.rawValue
	}
}

/**
	Enum for Searching
*/
enum ImageKey: String {
	case urlString
}
