//
//  User.swift
//  Net-a-Porter
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

/**
	A User object
*/
@objcMembers final class User: Object {
	
	///	An ID of the user
	dynamic var id: Int = -1
	
	///	A list of items this User has liked
	dynamic var liked = List<Clothing>()
	
	/**
		Basic initialiser
	*/
	convenience init(id: Int) {
		self.init()
		
		self.id = id
	}
	
	/**
		Primary Key
	*/
	override class func primaryKey() -> String {
		return UserKey.id.rawValue
	}
	
}

/**
	Keys for the User Object
*/
enum UserKey: String {
	case id
}
