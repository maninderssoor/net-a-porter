//
//  ViewController.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import UIKit
import Lottie
import SDWebImage

/**
	The Clothing List View controller presents the items of clothing
*/
final class ClothingListViewController: UIViewController, ClothingCellDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	
	// MARK: Identifiers
	
	///	Storyboard identifier
	static let identifier = "ClothingListViewController"
	
	// MARK: Variables
	
	///	View Model
	let viewModel = ListingViewModel()
	
	///	A User view model
	let userViewModel = UserViewModel()
	
	///	Activity animation view
	let activity = AnimationView()
	
	// MARK: UI
	
	///	The collection view
	@IBOutlet weak var collection: UICollectionView?
	
	// MARK: Lifecycle
	
	/**
		Setup the view
	*/
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupLoadingData(isLoading: true)
		setupCollectionView()
		setupLoadingView()
		loadData()
	}
	
	// MARK: Setup
	
	/**
		Setup collection view
	*/
	func setupCollectionView() {
		collection?.dataSource = self
		collection?.delegate = self
		
		collection?.register(UINib(nibName: ClothingCell.identifier, bundle: nil), forCellWithReuseIdentifier: ClothingCell.identifier)
		collection?.accessibilityIdentifier = UITests.collection.rawValue
	}
	
	/**
		Setup loading view
	*/
	func setupLoadingView() {
		activity.animation = Animation.named(Constants.activity.rawValue)
		activity.accessibilityIdentifier = UITests.activity.rawValue
		activity.contentMode = .scaleAspectFit
		activity.frame = view.frame
		view.addSubview(activity)
		activity.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
	}
	
	/**
		Animation for loading
	*/
	func setupLoadingData(isLoading loading: Bool, completion: ((Bool) -> Void)? = nil) {
		UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: { [weak self] in
			
			self?.activity.alpha = loading ? 1.0 : 0.0
			self?.collection?.alpha = loading ? 0.0 : 1.0
			
		}) { (isComplete) in
			completion?(isComplete)
		}
		
		if loading {
			activity.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
		} else {
			activity.pause()
		}
	}
	
	/**
		Load the data
	*/
	func loadData(completion: ((Bool) -> Void)? = nil) {
		viewModel.loadData { [weak self] (isSuccess) in
			
			guard let self = self, isSuccess else {
				print("No weak self or error loading data!!!")
				return
			}
			
			self.collection?.reloadData()
			self.setupLoadingData(isLoading: false)
		}
	}
	
	// MARK: UICollectionView Datasource
	
	/**
		Number of items
	*/
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return viewModel.numberOfItems()
	}
	
	/**
		Cell for item at index path
	*/
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ClothingCell.identifier, for: indexPath) as? ClothingCell else {
			preconditionFailure("Couldn't dequeue the Clothing Cell. Going to crash now!")
		}
		
		cell.reset()
		cell.delegate = self
		cell.clothing = viewModel.item(atIndexPath: indexPath)
		cell.labelName?.text = viewModel.nameForItem(atIndexPath: indexPath)
		cell.labelPrice?.text = viewModel.priceForItem(atIndexPath: indexPath)
		cell.buttonLiked?.setImage(#imageLiteral(resourceName: "Favourite"), for: .normal)
		if let clothing = viewModel.item(atIndexPath: indexPath), userViewModel.isLikedItem(ofClothing: clothing) {
			cell.buttonLiked?.setImage(#imageLiteral(resourceName: "Favourite_Selected"), for: .normal)
		}
		cell.imageClothing?.sd_cancelCurrentImageLoad()
		if let imageURL = viewModel.imageURLString(atIndexPath: indexPath) {
			cell.imageClothing?.sd_setImage(with: URL(string: imageURL), completed: { (_, _, _, _) in
				cell.setImage()
			})
		}
		
		return cell
	}
	
	/**
		Size for item
	*/
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let collectionWidth = collectionView.frame.width - collectionView.contentInset.right - collectionView.contentInset.left
		let sizeClass = view.traitCollection.horizontalSizeClass
		
		return viewModel.sizeForItem(atIndexPath: indexPath, givenWidth: collectionWidth, andSizeClass: sizeClass)
	}
	
	
	// MARK: ClothingCellDelegate
	
	/**
		Did toggle item
	*/
	func itemLikeTapped(withClothing clothing: Clothing) {
		if userViewModel.isLikedItem(ofClothing: clothing) {
			userViewModel.unlikeItem(ofClothing: clothing)
		} else {
			userViewModel.likeItem(ofClothing: clothing)
		}
		
		if let indexPath = viewModel.indexPath(ofItem: clothing) {
			collection?.reloadItems(at: [indexPath])
		}
	}
}

