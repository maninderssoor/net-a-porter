//
//  MovieListingAdapter.swift
//  MovieDB
//
//  Created by Maninder Soor on 06/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

/**
	The Clothing Adapter converts [APIClothing] into an array of [Clothing]
*/
final class ClothingAdapter {
	
	// MARK: Constants
	
	///	An array of APIClothing
	private let clothing: [APIClothing]
	
	/**
		Initialiser
	*/
	init(clothing: [APIClothing]) {
		self.clothing = clothing
	}
	
	/**
		Converts data and returns an [Clothing] Realm objects, if successful
	*/
	func convert() -> [Clothing]? {
		var dbClothing = [Clothing]()
		
		for thisClothing in self.clothing {
			
			///	For the purposes of this test the {{ xx }} values are being replaced.
			///	Given more time the shots/ sizes would be dynamic (stored as seperate Realm objects) and would need more info on the {{ scheme }} before solutionising.
			///	The image is optional, so the user can get information and at least discover the product.
			var image: Image?
			if let firstSize = thisClothing.images.sizes.first {
				let imageURLString = thisClothing.images.urlTemplate
					.replacingOccurrences(of: "{{scheme}}", with: "https:")
					.replacingOccurrences(of: "{{shot}}", with: "in")
					.replacingOccurrences(of: "{{size}}", with: firstSize)
				image = Image(urlString: imageURLString)
			}
			let price: Float = Float(thisClothing.price.amount) / Float(thisClothing.price.divisor)
			var symbol = ""
			let currency = NSLocale(localeIdentifier: thisClothing.price.currency)
			if let currencySymbol = currency.displayName(forKey: NSLocale.Key.currencySymbol, value: thisClothing.price.currency) {
				symbol = currencySymbol
			}
			
			let clothing: Clothing = Clothing(id: thisClothing.id,
											  name: thisClothing.name,
											  currency: thisClothing.price.currency,
											  currencySymbol: symbol,
											  price: price,
											  image: image)
			
			dbClothing.append(clothing)
		}
		
		return dbClothing.count > 0 ? dbClothing : nil
	}
}


