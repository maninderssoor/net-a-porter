//
//  ListingViewModel.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	The Clothing Listing View Model handles
*/
final class ListingViewModel {
	
	///	A network manager
	private let networkManager: Networking
	
	///	Formatter
	private let formatter = NumberFormatter()
	
	/// Clothing
	var clothing = [Clothing]()
	
	// MARK: Methods
	
	/**
		Initialiser
	*/
	init(networkingManager: Networking = NetworkManager()) {
		self.networkManager = networkingManager
		self.formatter.numberStyle = .decimal
	}
	
	/**
		Load data from the database and API
	*/
	func loadData(fromDatasources datasources: [DataSource] = [.api, .database], completion: ((Bool) -> Void)? = nil) {
		if datasources.contains(.database){
			fetchFromDatabase(completion: completion)
		}

		if datasources.contains(.api) {
			fetchFromAPI(completion: completion)
		}
	}
	
	/**
		Fetches from the database
	*/
	private func fetchFromDatabase(completion: ((Bool) -> Void)? = nil) {
		guard let clothing = Database.fetch(for: Clothing.self) as? [Clothing] else {
			print("Error fetching Clothing from the Database")
			completion?(false)
			return
		}
		
		///	Update cached data in the view model
		self.clothing.removeAll()
		self.clothing.append(contentsOf: clothing)
		
		///	Completion
		completion?(true)
	}
	
	/**
		Load the clothing data from the API.
	*/
	private func fetchFromAPI(completion: ((Bool) -> Void)? = nil) {
		let parameters = APIClothingParameteres(categoryID: 2)
		networkManager.fetch(with: APIClothingService.self, and: parameters) { [weak self](results) in
			
			guard let self = self, let service = results.service as? APIClothingService else {
				print("Error parsing the results or no weak self")
				completion?(false)
				return
			}
			
			///	Use an Adapter to map API -> DB models and update the DB
			let adapter = ClothingAdapter(clothing: service.clothing)
			guard let dbClothing = adapter.convert() else {
				print("Error converting the data")
				completion?(false)
				return
			}
			Database.addObjects(with: dbClothing)
			
			///	Refetch Movies
			self.fetchFromDatabase(completion: { (isSuccess) in
				completion?(isSuccess)
			})
		}
	}
	
	// MARK: UITableView Items
	
	/**
		Number of items
	*/
	func numberOfItems() -> Int {
		return clothing.count
	}
	
	/**
		Item at index path
	*/
	func item(atIndexPath indexPath: IndexPath) -> Clothing? {
		guard indexPath.row < clothing.count else {
			return nil
		}
		
		return clothing[indexPath.row]
	}
	
	/**
		Index path of item
	*/
	func indexPath(ofItem item: Clothing) -> IndexPath? {
		guard let firstIndex = clothing.firstIndex(where: { $0.id == item.id }) else {
			print("Couldn't find an item of clothing with Name \(item.name)")
			return nil
		}
		
		return IndexPath(item: firstIndex, section: 0)
	}
	
	/**
		Name for item at index path
	*/
	func nameForItem(atIndexPath indexPath: IndexPath) -> String? {
		guard let item = item(atIndexPath: indexPath) else {
			return nil
		}
		
		return item.name
	}
	
	/**
		Price for item at index path
	*/
	func priceForItem(atIndexPath indexPath: IndexPath) -> String? {
		guard let item = item(atIndexPath: indexPath) else {
			return nil
		}
		
		guard let formattedPrice = formatter.string(from: NSNumber(value: item.price)) else {
			print("Error formatting the price")
			return nil
		}
		
		return "\(item.currencySymbol) \(formattedPrice)"
	}
	
	/**
		Image URL for item at index path
	*/
	func imageURLString(atIndexPath indexPath: IndexPath) -> String? {
		guard let item = item(atIndexPath: indexPath) else {
			return nil
		}
		
		return item.image?.urlString
	}
	
	/**
		Size for item at index path
	*/
	func sizeForItem(atIndexPath indexPath: IndexPath, givenWidth width: CGFloat, andSizeClass sizeClass: UIUserInterfaceSizeClass) -> CGSize {
		
		var numberOfItems: CGFloat = 2.0
		if sizeClass == UIUserInterfaceSizeClass.regular {
			numberOfItems = 4.0
		}
		let cellWidth = width / numberOfItems
		return CGSize(width: cellWidth,
					  height: (cellWidth / 2) * 3)
	}
}
