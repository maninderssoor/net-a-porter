//
//  UserViewModel.swift
//  Net-a-Porter
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	The user view model
*/
final class UserViewModel {
	
	// MARK: Methods
	
	/**
		Fetch the default user.
	
		For now, since there's no login / registration, if a User isn't found it's created.
	*/
	func user() -> User? {
		if let user = Database.fetch(for: User.self)?.first as? User {
			return user
		}
		
		let newUser = User(id: 100)
		Database.addObjects(with: [newUser])
		let dbUser = Database.fetch(for: User.self)?.first as? User
		
		return dbUser
	}
	
	/**
		Checks if an item of clothing is liked
	*/
	func isLikedItem(ofClothing clothing: Clothing) -> Bool {
		let count = user()?.liked.filter({ $0.id == clothing.id }).count ?? 0
		return count > 0
	}
	
	/**
		Likes an item of clothing
	*/
	@discardableResult
	func likeItem(ofClothing clothing: Clothing) -> Bool {
		
		let user = self.user()
		user?.liked.appendAndWrite(clothing)
		
		return user != nil
	}
	
	/**
		Unlike an item of clothing
	*/
	@discardableResult
	func unlikeItem(ofClothing clothing: Clothing) -> Bool {
		
		let user = self.user()
		user?.liked.removeAndWrite(clothing)
		
		return user != nil
	}
}
