//
//  MovieCell.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit
import Lottie

/**
	Clothing Cell
*/
final class ClothingCell: UICollectionViewCell {
	
	// MARK: Identifier
	static let identifier = "ClothingCell"
	
	// MARK: Variables
	
	///	Animation view
	private var activity: AnimationView?
	
	///	Clothing item
	var clothing: Clothing?
	
	///	Delegate
	var delegate: ClothingCellDelegate?
	
	// MARK: Outlets
	
	///	The image view
	@IBOutlet weak var imageClothing: UIImageView?
	
	///	The name
	@IBOutlet weak var labelName: UILabel?
	
	///	The price
	@IBOutlet weak var labelPrice: UILabel?
	
	///	The liked buton
	@IBOutlet weak var buttonLiked: UIButton?
	
	// MARK: Setup
	
	/**
		Reset
	*/
	func reset() {
		if activity == nil {
			activity = AnimationView(animation: Animation.named(Constants.activity.rawValue))
			activity?.contentMode = .scaleAspectFit
			activity?.frame = imageClothing?.frame ?? contentView.frame
			
			if let unwrappedActivityView = activity {
				contentView.addSubview(unwrappedActivityView)
			}
			activity?.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
		}
		activity?.alpha = 1.0
		
		imageClothing?.alpha = 0.0
		imageClothing?.image = nil
		imageClothing?.accessibilityIdentifier = UITests.imageView.rawValue
		labelName?.text = ""
		labelPrice?.text = ""
	}
	
	/**
		Set image
	*/
	func setImage() {
		UIView.animate(withDuration: 0.3, animations: { [weak self] in
			self?.imageClothing?.alpha = 1.0
			self?.activity?.alpha = 0.0
		})
	}
	
	/**
		Toggle Like button
	*/
	@IBAction func toggleLike() {
		guard let item = clothing else {
			print("Couldn't get a clothing item")
			return
		}
		
		delegate?.itemLikeTapped(withClothing: item)
	}
}

protocol ClothingCellDelegate {
	func itemLikeTapped(withClothing clothing: Clothing)
}
