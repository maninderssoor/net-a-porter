//
//  APITests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import Net_a_Porter

/**
	API Tests, to check services are running correctly
*/
class APITests: Net_a_PorterTests {
	
	/// A networking manager
	let networkingManager = NetworkManager()
	
	///	A Mock network manager
	let mockNetworkManager = MockNetwork()
	
	/**
		Test connections API Call
	*/
	 func testConnectionsAPICall() {
		let asychronousExpectation = expectation(description: "testConnectionsAPICall")
		
		let parameters = APIClothingParameteres(categoryID: 2)
		networkingManager.fetch(with: APIClothingService.self, and: parameters) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertNotNil(results.service, "The service returned should not be nil")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test networking protocol
	*/
	public func testNetworkProtocol() {
		let asychronousExpectation = expectation(description: "testNetworkProtocol")
		
		let parameters = APIClothingParameteres(categoryID: 2)
		mockNetworkManager.fetch(with: APIClothingService.self, and: parameters) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertNotNil(results.service, "The service returned should not be nil")
			
			guard let items = results.service as? APIClothingService else {
				XCTFail("Error unwrapping the service as a Clothing Service")
				asychronousExpectation.fulfill()
				return
			}
			
			XCTAssertEqual(items.clothing.count, 3, "There should be 3 items of clothing from the Mock response")
			
			let firstItem = items.clothing[0]
			XCTAssertEqual(firstItem.id, 1125760, "The ID wasn't set correctly")
			XCTAssertEqual(firstItem.name, "Sunday striped ribbed stretch-knit midi dress", "The name wasn't set correctly")
			XCTAssertEqual(firstItem.price.currency, "GBP", "The currency wasn't set correctly")
			XCTAssertEqual(firstItem.price.amount, 64500, "The amount wasn't set correctly")
			XCTAssertEqual(firstItem.price.divisor, 100, "The divisor wasn't set correctly")
			XCTAssertEqual(firstItem.images.urlTemplate, "{{scheme}}//cache.net-a-porter.com/images/products/1125760/1125760_{{shot}}_{{size}}.jpg", "The URL template wasn't set correctly")
			
			let secondItem = items.clothing[1]
			XCTAssertEqual(secondItem.id, 1125761, "The ID wasn't set correctly")
			XCTAssertEqual(secondItem.name, "Sunday striped ribbed stretch-knit", "The name wasn't set correctly")
			XCTAssertEqual(secondItem.price.currency, "GB", "The currency wasn't set correctly")
			XCTAssertEqual(secondItem.price.amount, 4500, "The amount wasn't set correctly")
			XCTAssertEqual(secondItem.price.divisor, 10, "The divisor wasn't set correctly")
			XCTAssertEqual(secondItem.images.urlTemplate, "{{scheme}}//cache.net-a-porter.com/images/products/1125761/1125761_{{shot}}_{{size}}.jpg")
			
			let thirdItem = items.clothing[2]
			XCTAssertEqual(thirdItem.id, 1125762, "The ID wasn't set correctly")
			XCTAssertEqual(thirdItem.name, "Sunday striped ribbed", "The name wasn't set correctly")
			XCTAssertEqual(thirdItem.price.currency, "G", "The currency wasn't set correctly")
			XCTAssertEqual(thirdItem.price.amount, 6500, "The amount wasn't set correctly")
			XCTAssertEqual(thirdItem.price.divisor, 200, "The divisor wasn't set correctly")
			XCTAssertEqual(thirdItem.images.urlTemplate, "{{scheme}}//cache.net-a-porter.com/images/products/1125762/1125762_{{shot}}_{{size}}.jpg", "The URL template wasn't set correctly")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test connections no url string API Call
	*/
	public func testNoURLConnectionsAPICall() {
		let asychronousExpectation = expectation(description: "testNoURLConnectionsAPICall")
		
		let emptyParameters = APITestParameters()
		networkingManager.fetch(with: APITestNoURL.self, and: emptyParameters) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test networking manager bad API
	*/
	public func testBadAPICall() {
		let asychronousExpectation = expectation(description: "testBadAPICall")
		
		let emptyParameters = APITestParameters()
		networkingManager.fetch(with: APITestBadURL.self, and: emptyParameters) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertFalse(results.isSuccess, "The call shouldn't have been a success")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
}


/**
	No URL
*/
public struct APITestNoURL: APIProtocol {
	
	/// The name of this API call, for logging
	static public var title: String = "NO URL"
	
	/// The URL for this call
	static public var urlString: String = ""
	
	///	If the call is fetching
	public static var isFetching: Bool = false
	
	/// Parameters
	public func parameters<E>(element: E) -> String? where E : APIProtocolParameters {
		return element.parameters()
	}
}


/**
	Fetch connection API
*/
public struct APITestBadURL: APIProtocol {
	
	/// The name of this API call, for logging
	static public var title: String = "BAD URL"
	
	/// The URL for this call
	static public var urlString: String = "http://someurlthatwontwork.com"
	
	///	If the call is fetching
	public static var isFetching: Bool = false
	
	/// Parameters
	public func parameters<E>(element: E) -> String? where E : APIProtocolParameters {
		return element.parameters()
	}
}

/**
	Sample parameters
*/
public struct APITestParameters: APIProtocolParameters {
	
	/**
		Return
	*/
	public func parameters() -> String? {
		return nil
	}
}
