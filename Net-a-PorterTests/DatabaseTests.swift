//
//  DatabaseTests.swift
//  Net-a-PorterTests
//
//  Created by Maninder Soor on 14/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import Net_a_Porter

/**
	Database Tests
*/
class DatabaseTests: Net_a_PorterTests {

	/**
		Test deletion without results
	*/
	func testDeletionWithoutResults() {
		Database.delete(forType: User.self)
		let results = Database.delete(forType: User.self)
		XCTAssertFalse(results, "This should be false")
	}
}
