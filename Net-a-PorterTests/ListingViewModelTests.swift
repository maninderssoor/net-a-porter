//
//  UserViewModelTests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import Net_a_Porter

/**
	Listing View Model Tests
*/
final class ListingViewModelTests: Net_a_PorterTests {
	
	///	A Listing View Model
	let listingViewModel = ListingViewModel(networkingManager: MockNetwork())
	
	///	Empty listing view model
	let emptyListingViewModel = ListingViewModel(networkingManager: MockEmptyNetwork())
	
	/**
		Test Listing view controller data
	*/
	func testListingCount() {
		let asychronousExpectation = expectation(description: "testListingData")
		Database.delete(forType: Clothing.self)
		listingViewModel.loadData(fromDatasources: [.api]) {[weak self](isSuccess) in
			XCTAssertNotNil(self, "Self should not be nil")
			XCTAssertTrue(isSuccess, "The listing view model should be a success")

			guard let self = self else {
				XCTFail("No weak self. Already been checked. What!")
				asychronousExpectation.fulfill()
				return
			}
			
			XCTAssertEqual(self.listingViewModel.clothing.count, 3, "There should be 3 items")
			XCTAssertEqual(self.listingViewModel.numberOfItems(), 3, "There should be 3 items")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test Listing name
	*/
	func testListingName() {
		let asychronousExpectation = expectation(description: "testListingName")
		Database.delete(forType: Clothing.self)
		listingViewModel.loadData(fromDatasources: [.api]) {[weak self](isSuccess) in
			guard let self = self else {
				XCTFail("No weak self. Already been checked. What!")
				asychronousExpectation.fulfill()
				return
			}
			
			let firstIndexPath = IndexPath(item: 0, section: 0)
			XCTAssertEqual(self.listingViewModel.nameForItem(atIndexPath: firstIndexPath), "Sunday striped ribbed stretch-knit midi dress", "Not returning the correct name")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test listing price
	*/
	func testListingPrice() {
		let asychronousExpectation = expectation(description: "testListingPrice")
		Database.delete(forType: Clothing.self)
		listingViewModel.loadData(fromDatasources: [.api]) {[weak self](isSuccess) in
			guard let self = self else {
				XCTFail("No weak self. Already been checked. What!")
				asychronousExpectation.fulfill()
				return
			}
			
			let firstIndexPath = IndexPath(item: 0, section: 0)
			XCTAssertEqual(self.listingViewModel.priceForItem(atIndexPath: firstIndexPath), "£ 645", "Not returning the correct price")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test listing image URL
	*/
	func testListingImageURL() {
		let asychronousExpectation = expectation(description: "testListingImageURL")
		Database.delete(forType: Clothing.self)
		listingViewModel.loadData(fromDatasources: [.api]) {[weak self](isSuccess) in
			guard let self = self else {
				XCTFail("No weak self. Already been checked. What!")
				asychronousExpectation.fulfill()
				return
			}
			
			let firstIndexPath = IndexPath(item: 0, section: 0)
			XCTAssertEqual(self.listingViewModel.imageURLString(atIndexPath: firstIndexPath), "https://cache.net-a-porter.com/images/products/1125760/1125760_in_dl.jpg", "Not returning the correct imageURL")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test Listing index path
	*/
	func testListingIndexPath() {
		let asychronousExpectation = expectation(description: "testListingIndexPath")
		Database.delete(forType: Clothing.self)
		listingViewModel.loadData(fromDatasources: [.api]) {[weak self](isSuccess) in
			
			guard let self = self else {
				XCTFail("No weak self. Already been checked. What!")
				asychronousExpectation.fulfill()
				return
			}
			
			guard let clothing = self.listingViewModel.clothing.first else {
				XCTFail("Couldn't get the first item of clothing")
				asychronousExpectation.fulfill()
				return
			}
			guard let indexPathOfItem = self.listingViewModel.indexPath(ofItem: clothing) else {
				XCTFail("There should be an index path for this item of clothing")
				asychronousExpectation.fulfill()
				return
			}
			XCTAssertEqual(indexPathOfItem.item, 0, "This should be the first item")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test size for item
	*/
	func testSizeForItem() {
		let asychronousExpectation = expectation(description: "testSizeForItem")
		Database.delete(forType: Clothing.self)
		listingViewModel.loadData(fromDatasources: [.api]) {[weak self](isSuccess) in
			
			guard let self = self else {
				XCTFail("No weak self. Already been checked. What!")
				asychronousExpectation.fulfill()
				return
			}
			let indexPath = IndexPath(row: 0, section: 0)
			let width: CGFloat = 1000.0
			let sizeForItem = self.listingViewModel.sizeForItem(atIndexPath: indexPath, givenWidth: width, andSizeClass: .regular)
			
			let calculatedWidth = width / 4.0
			XCTAssertEqual(sizeForItem.width, calculatedWidth, "The width wasn't caculated correctly for mobile landscape")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test Listing view controller data when empty results
	*/
	func testEmptyListingData() {
		let asychronousExpectation = expectation(description: "testEmptyListingData")
		Database.delete(forType: Clothing.self)
		emptyListingViewModel.loadData(fromDatasources: [.api]) { [weak self] (isSuccess) in
			guard let self = self else {
				XCTFail("No weak self. ")
				asychronousExpectation.fulfill()
				return
			}
			
			let firstIndexPath = IndexPath(item: 0, section: 0)
			XCTAssertEqual(self.emptyListingViewModel.clothing.count, 0, "There shouldn't be any items")
			XCTAssertEqual(self.emptyListingViewModel.numberOfItems(), 0, "There shouldn't be any items")
			XCTAssertNil(self.emptyListingViewModel.nameForItem(atIndexPath: firstIndexPath), "The name should be nil")
			XCTAssertNil(self.emptyListingViewModel.priceForItem(atIndexPath: firstIndexPath), "The price should be nil")
			XCTAssertNil(self.emptyListingViewModel.imageURLString(atIndexPath: firstIndexPath), "The image should be nil")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test empty list item name
	*/
	func testEmptyListItemName() {
	let asychronousExpectation = expectation(description: "testEmptyListItemName")
	Database.delete(forType: Clothing.self)
	emptyListingViewModel.loadData(fromDatasources: [.api]) { [weak self] (isSuccess) in
		guard let self = self else {
			XCTFail("No weak self. ")
			asychronousExpectation.fulfill()
			return
		}
		
		let firstIndexPath = IndexPath(item: 0, section: 0)
		XCTAssertNil(self.emptyListingViewModel.nameForItem(atIndexPath: firstIndexPath), "The name should be nil")
		
		asychronousExpectation.fulfill()
	}
	
	waitForExpectations(timeout: timeout) { (error) in
		if let error = error {
			print("There was an error with \(String(describing: error))")
			XCTFail("The asychronousExpectation timed out")
		}
	}
}
	
	/**
		Test empty list item price
	*/
	func testEmptyListPrice() {
	let asychronousExpectation = expectation(description: "testEmptyListPrice")
	Database.delete(forType: Clothing.self)
	emptyListingViewModel.loadData(fromDatasources: [.api]) { [weak self] (isSuccess) in
		guard let self = self else {
			XCTFail("No weak self. ")
			asychronousExpectation.fulfill()
			return
		}
		
		let firstIndexPath = IndexPath(item: 0, section: 0)
		XCTAssertNil(self.emptyListingViewModel.priceForItem(atIndexPath: firstIndexPath), "The price should be nil")
		
		asychronousExpectation.fulfill()
	}
	
	waitForExpectations(timeout: timeout) { (error) in
		if let error = error {
			print("There was an error with \(String(describing: error))")
			XCTFail("The asychronousExpectation timed out")
		}
	}
}
	
	/**
		Test empty list item imageURL
	*/
	func testEmptyListImageURL() {
		let asychronousExpectation = expectation(description: "testEmptyListImageURL")
		Database.delete(forType: Clothing.self)
		emptyListingViewModel.loadData(fromDatasources: [.api]) { [weak self] (isSuccess) in
			guard let self = self else {
				XCTFail("No weak self. ")
				asychronousExpectation.fulfill()
				return
			}
			
			let firstIndexPath = IndexPath(item: 0, section: 0)
			XCTAssertNil(self.emptyListingViewModel.imageURLString(atIndexPath: firstIndexPath), "The image should be nil")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test index path of bad clothing item
	*/
	func testIndexPathOfBadClothing() {
		let asychronousExpectation = expectation(description: "testEmptyListingData")
		let emptyViewModel = ListingViewModel(networkingManager: MockEmptyNetwork())
		Database.delete(forType: Clothing.self)
		emptyViewModel.loadData(fromDatasources: [.api]) {(isSuccess) in
			
			let sampleClothing = Clothing(id: 1, name: "Name", currency: "GBP", currencySymbol: "£", price: 1.0, image: nil)
			XCTAssertNil(emptyViewModel.indexPath(ofItem: sampleClothing), "This should be nil")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
}
