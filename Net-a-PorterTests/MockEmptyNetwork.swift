//
//  File.swift
//  Net-a-PorterTests
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import Net_a_Porter

/**
	A Mock Network that returns empty results
*/
final class MockEmptyNetwork: Networking {
	
	/**
		Returns an array of Clothing items
	*/
	func fetch<E, P>(with type: E.Type, and parameters: P, completion: @escaping ((APIProtocolCompletion) -> Void)) where E : APIProtocol, P : APIProtocolParameters {
		
		let service = APIClothingService(clothing: [])
		completion(APIProtocolCompletion(isSuccess: true, service: service))
	}
	
}
