//
//  MockNetwork.swift
//  Net-a-PorterTests
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import Net_a_Porter

/**
	A Mock Network
*/
final class MockNetwork: Networking {
	
	/**
		Returns an array of Clothing items
	*/
	func fetch<E, P>(with type: E.Type, and parameters: P, completion: @escaping ((APIProtocolCompletion) -> Void)) where E : APIProtocol, P : APIProtocolParameters {
		
		let image1 = APIImages(shots: ["in","ou","fr","bk","cu"], sizes: ["dl","l","m","m2","mt2","pp","s","sl","xs", "xxl"], urlTemplate: "{{scheme}}//cache.net-a-porter.com/images/products/1125760/1125760_{{shot}}_{{size}}.jpg")
		let image2 = APIImages(shots: ["in","ou","fr","bk","cu"], sizes: ["dl","l","m","m2","mt2","pp","s","sl","xs", "xxl"], urlTemplate: "{{scheme}}//cache.net-a-porter.com/images/products/1125761/1125761_{{shot}}_{{size}}.jpg")
		let image3 = APIImages(shots: ["in","ou","fr","bk","cu"], sizes: ["dl","l","m","m2","mt2","pp","s","sl","xs", "xxl"], urlTemplate: "{{scheme}}//cache.net-a-porter.com/images/products/1125762/1125762_{{shot}}_{{size}}.jpg")
		
		let price1 = APIPrice(currency: "GBP", divisor: 100, amount: 64500)
		let price2 = APIPrice(currency: "GB", divisor: 10, amount: 4500)
		let price3 = APIPrice(currency: "G", divisor: 200, amount: 6500)
		
		let clothing1 = APIClothing(id: 1125760, name: "Sunday striped ribbed stretch-knit midi dress", price: price1, isVisible: true, images: image1)
		let clothing2 = APIClothing(id: 1125761, name: "Sunday striped ribbed stretch-knit", price: price2, isVisible: true, images: image2)
		let clothing3 = APIClothing(id: 1125762, name: "Sunday striped ribbed", price: price3, isVisible: true, images: image3)
		
		let service = APIClothingService(clothing: [clothing1, clothing2, clothing3])
		
		completion(APIProtocolCompletion(isSuccess: true, service: service))
	}
	
}
