//
//  Net_a_PorterTests.swift
//  Net-a-PorterTests
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import Net_a_Porter

class Net_a_PorterTests: XCTestCase {

	///	Timeout for all asynchronous calls
	public let timeout = 120.0
	
    override func setUp() {
		cleanupDatabase()
    }

    override func tearDown() {
		cleanupDatabase()
    }
	
	private func cleanupDatabase() {
		Database.delete(forType: Clothing.self)
		Database.delete(forType: User.self)
	}
}
