//
//  UserViewModelTests.swift
//  Net-a-PorterTests
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
import Realm
import RealmSwift
@testable import Net_a_Porter

/**
	User View Model Tests
*/
final class UserViewModelTests: Net_a_PorterTests {
	
	///	User view model
	let userViewModel = UserViewModel()
	
	///	Sample clothing
	let clothing = Clothing(id: 100, name: "Name", currency: "GBP", currencySymbol: "£", price: 100.0, image: nil)
	
	/**
		Test user creation
	*/
	func testUserCreation() {
		Database.delete(forType: User.self)
		let user = userViewModel.user()
		
		XCTAssertNotNil(user, "The user should not be nil")
	}
	
	/**
		Test user ID
	*/
	func testUserID() {
		Database.delete(forType: User.self)
		guard let user = userViewModel.user() else {
			XCTFail("A User should have been returned")
			return
		}
		XCTAssertEqual(user.id, 100, "The ID should be 100")
	}
	
	/**
		Test if an item is liked
	*/
	func testItemIsLiked() {
		Database.delete(forType: User.self)
		Database.addObjects(with: [clothing])
		XCTAssertFalse(userViewModel.isLikedItem(ofClothing: clothing), "The item shouldn't be liked")
	}
	
	/**
		Test liking of clothing item
	*/
	func testItemLiked() {
		Database.delete(forType: User.self)
		Database.addObjects(with: [clothing])
		XCTAssertTrue(userViewModel.likeItem(ofClothing: clothing), "The item of clothing should have been liked")
		XCTAssertTrue(userViewModel.isLikedItem(ofClothing: clothing), "The item should be liked now")
	}
	
	/**
		Test unliking an item
	*/
	func testItemUnlike() {
		Database.delete(forType: User.self)
		Database.addObjects(with: [clothing])
		userViewModel.likeItem(ofClothing: clothing)
		
		XCTAssertTrue(userViewModel.unlikeItem(ofClothing: clothing), "The item of clothing should have been liked")
		XCTAssertFalse(userViewModel.isLikedItem(ofClothing: clothing), "The item should be liked now")
		
	}
	
}
