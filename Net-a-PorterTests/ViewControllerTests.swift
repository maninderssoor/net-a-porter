//
//  ViewControllerTests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import Net_a_Porter

/**
	View Controller tests
*/
class ViewControllerTests: Net_a_PorterTests {
	
	///	Identifier
	let bundleID = "com.manindersoor.Net-a-Porter"
	
	///	Storyboard
	let storyboard = UIStoryboard(name: "Main", bundle: Bundle(identifier: "com.manindersoor.Net-a-Porter"))
	
	/**
		Test liked action
	*/
	public func testLiked() {
		guard let navigation = storyboard.instantiateInitialViewController() as? UINavigationController,
		let controller = navigation.viewControllers.first as? ClothingListViewController else {
			XCTFail("The controller wasn't initialised correctly")
			return
		}
		
		let userViewModel = UserViewModel()
		let sampleClothing = Clothing(id: 100, name: "", currency: "", currencySymbol: "", price: 100.0, image: nil)
		
		Database.delete(forType: User.self)
		Database.addObjects(with: [sampleClothing])
		XCTAssertFalse(userViewModel.isLikedItem(ofClothing: sampleClothing), "This item of clothing shouldn't be liked right now")
		controller.itemLikeTapped(withClothing: sampleClothing)
		XCTAssertTrue(userViewModel.isLikedItem(ofClothing: sampleClothing), "The item should be liked now")
		controller.itemLikeTapped(withClothing: sampleClothing)
		XCTAssertFalse(userViewModel.isLikedItem(ofClothing: sampleClothing), "The item shouldn't be liked any more")

	}
	
	/**
		Test clothing liked
	*/
	public func testClothingLiked() {
		guard let xib = UINib(nibName: ClothingCell.identifier, bundle: Bundle(identifier: bundleID)).instantiate(withOwner: nil, options: nil).first as? ClothingCell else {
			XCTFail("Couldn't initalise the nib view")
			return
		}
		
		let sampleClothing = Clothing(id: 200, name: "", currency: "", currencySymbol: "", price: 100.0, image: nil)
		xib.clothing = sampleClothing
		xib.prepareForReuse()
		xib.toggleLike()
	}
	
	/**
		Test cell liked without clothing
	*/
	public func testCellLiked() {
		guard let xib = UINib(nibName: ClothingCell.identifier, bundle: Bundle(identifier: bundleID)).instantiate(withOwner: nil, options: nil).first as? ClothingCell else {
			XCTFail("Couldn't initalise the nib view")
			return
		}
		
		xib.prepareForReuse()
		xib.toggleLike()
	}
}
