//
//  Net_a_PorterUITests.swift
//  Net-a-PorterUITests
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
import SDWebImage
@testable import Net_a_Porter

class Net_a_PorterUITests: XCTestCase {

	///	Application
	let application = XCUIApplication()
	
	///	Timeout
	let timeout: TimeInterval = 320.0
	
	///	Exist predicate
	let existsPredicate = NSPredicate(format: "exists == 1")
	
	///	Doesnt exist predicate
	let doesntExistPredicate = NSPredicate(format: "exists == 0")
	
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        application.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

	/**
		Test the loading of data
	*/
	func testBasicLoading() {
		
		///	Initial state
		let activityVisible = expectation(for: existsPredicate,
										  evaluatedWith: application.otherElements.matching(identifier: UITests.activity.rawValue),
										  handler: nil)
		let collectionHidden = expectation(for: doesntExistPredicate,
									  evaluatedWith: application.collectionViews.element(matching: XCUIElement.ElementType.table, identifier: UITests.collection.rawValue) ,
									  handler: nil)
		
		/// Switched
		let activityHidden = expectation(for: doesntExistPredicate,
										 evaluatedWith: application.otherElements.matching(identifier: UITests.activity.rawValue),
										 handler: nil)
		let collectionVisible = expectation(for: existsPredicate,
									   evaluatedWith: application.collectionViews.element(matching: XCUIElement.ElementType.table, identifier: UITests.collection.rawValue) ,
									   handler: nil)
		
		XCTWaiter().wait(for: [activityVisible, collectionHidden,
							   activityHidden, collectionVisible],
						 timeout: timeout,
						 enforceOrder: true)
	}
	
	/**
		Check header title
	*/
	func testHeaderTitle() {
		let titleText = application.staticTexts.element(matching: XCUIElement.ElementType.staticText, identifier: UITests.navigationHeaderLabel.rawValue)
		XCTAssert(titleText.exists, "The navigation header should exit")
		
		let text = application.staticTexts["CLOTHING"]
		XCTAssertEqual(text.identifier, UITests.navigationHeaderLabel.rawValue, "The header isn't correctly labelled")
	}

	/**
		Tests all images have successfully loaded
	*/
	func testImagesLoaded() {
		SDImageCache.shared.clearMemory()
		SDImageCache.shared.clearDisk(onCompletion: nil)
		var expectations = [XCTestExpectation]()
		
		///	Loop each of the items and check
		///	Assumption that we'll always get 60 items in this sample API feed.
		var count = 0
		for thisIndex in 0..<60 {
			
			let cell = application.collectionViews[UITests.collection.rawValue].cells.element(boundBy: thisIndex)
			let imageVisible = cell.images[UITests.imageView.rawValue]
			let imageVisibleExpectation = expectation(for: existsPredicate, evaluatedWith: imageVisible, handler: nil)
			expectations.append(imageVisibleExpectation)
			
			if count % 4 == 0 {
				application.collectionViews[UITests.collection.rawValue].swipeUp()
			}
			count += 1
		}
		
		///	Run the test
		XCTWaiter().wait(for: expectations,
						 timeout: timeout,
						 enforceOrder: true)
	}
}
