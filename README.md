# Net - A - Porter

[![Carthage Compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)
![Code Coverage](https://img.shields.io/badge/coverage-95%25-green.svg)
[![iOS Platform](https://img.shields.io/badge/platform-ios-lightgrey.svg)](https://img.shields.io/badge/platform-ios-lightgrey.svg)

## Requirments:

You will build a product listing app for our main fashion brand: Net-A-Porter. The app will allow users to browse Net-A-Porter’s Clothing category. The app will contain a list of clothing products with basic information displayed for every product. 

* Support iOS 11.0 and greater
* Using Swift
* The data regarding the products to be displayed should be fetched from the API provided
* Each item in the product list should contain: 
* The product name
* The price of the product (in GBP)
* One image representing the product

Bonus tasks:

* Please approach the following tasks only after you have completed the initial task. In the product list view, implement a way of marking a product as “favourite” that will persist between different executions of the app.
* Implement a system to persist the images you will download for the products on the website, and make it so that an image is only downloaded from the website if it is not already available locally.


## Notes

This sample app uses Net-a-Porter's sample API to fetch products and cache them in a Realm database.

API fetching uses the APIProtocol to create services via a Networking protocol using generics to create the fetch request alongside paramters (APIProtocolParameters).

Data is requested by the view controller and the ListingViewModel fetches from the DB and updates from the API using the service above. An adapter converts the API data to DB types and caches them in a Realm, some assumptions have been made (documented in code), e.g. Locale is used to fetch the currency symbol.

A sample user has been created (assumptions in code) for which liked clothing items are show.

I've used SDWebImage for caching; mainly for it's cancel/ set image features (and in built caching), though images could be downloaded in the caches directory and stored in a NSCache during app init if done without a library.

As it's a UICollectionView size classes have been used for more cells on mobile landscape, tablet and tablet landscape.

3rd Party Libraries:

- SDWebImage: Caching of images with operation queue downloading rather than using 'URLSession'.
- Realm: Database caching; happy to use Core Data though Realm is fairly lightweight.
- Lottie: UI for loading animation (nice to have)

Unit testing has been done throughout with code coverage of 95%.

## System Requirements

- iOS 10.0+
- Xcode 10.0+
- Swift 4.0
- Command Line Tools 10.0+

## Documentation

Code has been document to a minumum of:

* Classes
* Functions
* Properties

Documentation can re-generated using jazz docs [Jazzy](https://github.com/realm/jazzy)

## Build

### Instructions

1. Checkout the application via Github or via a terminal.
2. Run `carthage update --platform iOS`
3. Open the application in Xcode and run.

## Author

Maninder Soor (http://manindersoor.com)
